# Config Files

A collection of config files for use on Linux machines. And stuff.

## Symlinks, yo

Best way to set these up and ensure your local config is up to date with the repo? Symlinks. OBSERVE:

```ln -s /path/to/repo/configFile /path/to/link```

`ln` creates a symbolic link that the OS and programs view as a file, except it just points to the source in the repo. This prevents you from having to copy shit around every time you pull and hope you put every config file in the right folder. Set em up once, never worry about it again. Below are the paths to where the symlinks for each program should live.

### i3
~/.config/i3/config

### URxvt
~/.Xdefaults

### VIm
~/.vimrc

### i3status
~/.config/i3status/config
