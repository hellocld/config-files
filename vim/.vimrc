set nocompatible	" required
filetype off		" required

" Python formatting goodness
au BufNewFile,BufRead *.py
	\ set tabstop=4 |
	\ set softtabstop=4 |
	\ set shiftwidth=4 |
	\ set textwidth=79 |
	\ set expandtab |
	\ set autoindent |
	\ set fileformat=unix

" Flag unnecessary whitespace - BROKEN
" au BufNewFile,BufRead *.py, *.pyw, *.c, *.h match BadWhitespace /\s\+$/

" UTF-8 support
set encoding=utf-8

" Set Vundle runtime path and init
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Vundle gonna manage Vundle
Plugin 'gmarik/Vundle.vim'

" Mah plugins
" vvvvvvvvv

Plugin 'tmhedberg/SimpylFold'		" Code folding
Bundle 'Valloric/YouCompleteMe'
Plugin 'vim-syntastic/syntastic'
Plugin 'altercation/vim-colors-solarized'	"Altercation theme
Plugin 'scrooloose/nerdtree'		" File browser-ish
Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim'}
Plugin 'junegunn/goyo.vim'		" Nice, clean viewing mode for writing
Plugin 'reedes/vim-pencil'		" Makes writing in Goyo much nicer

" ^^^^^^^^^

call vundle#end()		" required
filetype plugin indent on	" required

" YouCompleteMe config
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g :YcmCompleter GoToDefinitionElseDeclaration<CR>

" Python virtualenv support
python3 << EOF
import os
import sys
if 'VIRTUAL_ENV' in os.environ:
	project_base_dir = os.environ['VIRTUAL_ENV']
	activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
	execfile(activate_this, dict(__file__=activate_this))
EOF

" Pretty python
let python_highlight_all=1
syntax on

" Color scheme based on GUI
set background=dark
colorscheme solarized

set nu

" Enable powerline ALL THE TIME
set laststatus=2
